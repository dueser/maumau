CREATE TABLE spiel
(
    id               SERIAL NOT NULL,
    start            TIMESTAMP,
    ende             TIMESTAMP,
    gewinnerId       INT,
    PRIMARY KEY (id)
);

CREATE TABLE spieler
(
    id               UUID NOT NULL,
    name             VARCHAR(32) NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE spiel_spieler
(
    spielId        INT NOT NULL,
    spielerId      UUID NOT NULL,
    position       SMALLINT NOT NULL
);

CREATE TABLE spielzug
(
    spielId          INT NOT NULL,
    karte            VARCHAR(10),
    von              INT,
    nach             INT,
    zugnummer        INT NOT NULL,
    spielerwechsel   BOOLEAN NOT NULL,
    zusatzinfo       VARCHAR(64),
    erstellt         TIMESTAMP NOT NULL
);
