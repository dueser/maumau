package eu.dueser.maumau.backend.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

@Data
public class Kartensammlung {

    public static Kartensammlung neuesGemischtenSpiel() {
        var kartensammlung = new Kartensammlung();
        for (FarbeEnum farbe : FarbeEnum.values()) {
            for (WertEnum wert : WertEnum.values()) {
                kartensammlung.hinzufuegen(new Karte(wert, farbe));
            }
        }
        var random = new Random(System.currentTimeMillis());
        for (int i = 0; i < 50; i++) {
            int index = random.nextInt(kartensammlung.anzahl());
            kartensammlung.hinzufuegen(kartensammlung.entnehmen(index));
        }
        return kartensammlung;
    }

    private final List<Karte> karten = new ArrayList<>();

    public boolean entfernen(Karte karte) {
        return karten.remove(karte);
    }

    public void hinzufuegen(Karte karte) {
        karten.add(karte);
    }

    public Karte entnehmen() {
        return karten.size() == 0? null : karten.remove(0);
    }

    public Karte entnehmen(int index) {
        return karten.size() == 0? null : karten.remove(index);
    }

    public Karte oberste() {
        return karten.size() == 0? null : karten.get(0);
    }

    public boolean istEnthalten(Karte karte) {
        return karten.contains(karte);
    }

    public int anzahl() {
        return karten.size();
    }


    public static void main(String[] args) {
        var kartensammlung = Kartensammlung.neuesGemischtenSpiel();
        System.out.println(kartensammlung);

    }
}
