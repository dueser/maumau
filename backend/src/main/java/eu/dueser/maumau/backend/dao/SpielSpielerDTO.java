package eu.dueser.maumau.backend.dao;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class SpielSpielerDTO {
    private long spielId;

    private UUID spielerId;

    private int position;

}



