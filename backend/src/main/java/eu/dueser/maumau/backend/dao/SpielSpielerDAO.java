package eu.dueser.maumau.backend.dao;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

@AllArgsConstructor
@Component
public class SpielSpielerDAO {

    private final DataSource dataSource;

    public long insertSpielSpieler(SpielSpielerDTO spielSpielerDTO) {
        var sql = "INSERT INTO spiel_spieler(spielId, spielerId,position) VALUES (?, ?, ?)";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql, RETURN_GENERATED_KEYS)
        ) {
            stmt.setLong(1, spielSpielerDTO.getSpielId());
            stmt.setObject(2, spielSpielerDTO.getSpielerId());
            stmt.setInt(3, spielSpielerDTO.getPosition());

            if (stmt.executeUpdate() == 0) {
                throw new SQLException("Creating 'spiel_spieler' failed, no rows affected.");
            }
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getLong(1);
                }
                throw new SQLException("Creating 'spiel_spieler' failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public List<SpielSpielerDTO> findSpielSpielerBySpielId(long spielId) {
        var sql = "SELECT * FROM spiel_spieler WHERE spielid = ?";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql)
        ) {
            stmt.setLong(1, spielId);

            try (ResultSet rs = stmt.executeQuery()) {
                var result = new ArrayList<SpielSpielerDTO>();
                while (rs.next()) {
                    result.add(SpielSpielerDTO.builder()
                            .spielId(rs.getLong("spielId"))
                            .spielerId(rs.getObject("spielerId", UUID.class))
                                    .position(rs.getInt("position"))
                            .build());
                }
                return result;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
