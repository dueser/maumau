package eu.dueser.maumau.backend.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Spielzustand {

    public final static int INDEX_ZIEHSTAPEL = -1;
    public final static int INDEX_ABLEGESTAPEL = -2;

    private int positionAktSpieler = 0;
    private boolean imUhrzeigersinn = true;
    private Kartensammlung ablagestapel;
    private Kartensammlung ziehstapel;
    private Kartensammlung[] spielerhaende;
    private Spiel spiel;

    public Spielzustand(Spiel spiel) {
        this.spiel = spiel;
        var anzahlSpieler = spielerAnzahl();
        spielerhaende = new Kartensammlung[anzahlSpieler];
        for (int i = 0; i < anzahlSpieler; i++) {
            spielerhaende[i] = new Kartensammlung();
        }
        ablagestapel = new Kartensammlung();
        ziehstapel = new Kartensammlung();
    }

    public void applySpielzug(Spielzug spielzug) {
        var nach = spielzug.getNach();
        var von = spielzug.getVon();
        var karte = spielzug.getKarte();
        var positionInkrement = 1;
        if (nach != null) {
            switch (nach) {
                case INDEX_ZIEHSTAPEL -> ziehstapel.hinzufuegen(karte);
                case INDEX_ABLEGESTAPEL -> {
                    ablagestapel.hinzufuegen(karte);
                    switch (karte.getWert()) {
                        case Z8 -> positionInkrement = 2;
                        case Z9 -> imUhrzeigersinn = !imUhrzeigersinn;
                    }
                }
                default -> spielerhaende[nach].hinzufuegen(karte);
            }
        }
        if (von != null) {
            switch (von) {
                case INDEX_ZIEHSTAPEL -> ziehstapel.entfernen(karte);
                case INDEX_ABLEGESTAPEL -> ablagestapel.entfernen(karte);
                default -> spielerhaende[von].entfernen(karte);
            }
        }

        if (spielzug.isSpielerwechsel()) {
            if (!imUhrzeigersinn) positionInkrement = -positionInkrement;
            positionAktSpieler = (positionAktSpieler + positionInkrement) % spielerAnzahl();
        }
    }

    public Kartensammlung getSpielerhand(UUID spielerId) {
        for (int i = 0; i < spielerAnzahl(); i++) {
            if (getSpieler(i).getId().equals(spielerId)) {
                return spielerhaende[i];
            }
        }
        return null;
    }

    public int spielerAnzahl() {
        return spiel.getSpielerliste().size();
    }

    public Spieler getSpieler(int position) {
        return spiel.getSpielerliste().get(position);
    }

    public Spieler aktuellerSpieler() {
        return getSpieler(positionAktSpieler);
    }

}



