package eu.dueser.maumau.backend.dao;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

@AllArgsConstructor
@Component
public class SpielzugDAO {

    private final DataSource dataSource;

    public long insertSpielzug(SpielzugDTO spielzug) {
        var sql = "INSERT INTO spielzug (spielId,karte,von,nach,zugnummer,spielerwechsel,zusatzinfo,erstellt) " +
                "VALUES (?, ?, ?, ?, ?, ?, ?, ?)";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql, RETURN_GENERATED_KEYS)
        ) {
            stmt.setLong(1, (spielzug.getSpielId()));
            stmt.setString(2, spielzug.getKarte());
            stmt.setObject(3, spielzug.getVon());
            stmt.setObject(4, spielzug.getNach());
            stmt.setInt(5, spielzug.getZugnummer());
            stmt.setBoolean(6, spielzug.isSpielerwechsel());
            stmt.setString(7, spielzug.getZusatzinfo());
            stmt.setTimestamp(8, Timestamp.from(spielzug.getErstellt()));

            if (stmt.executeUpdate() == 0) {
                throw new SQLException("Creating 'spielzug' failed, no rows affected.");
            }
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getLong(1);
                }
                throw new SQLException("Creating 'spielzug' failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<SpielzugDTO> findSpielzuegeBySpielId(long spielId) {
        var sql = "SELECT * FROM spielzug WHERE spielid = ? ORDER BY zugnummer";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql)
        ) {
            stmt.setLong(1, spielId);

            try (ResultSet rs = stmt.executeQuery()) {
                var spielzugDTOList = new ArrayList<SpielzugDTO>();
                while (rs.next()) {

                    var spielzugDTO = SpielzugDTO.builder()
                            .spielId(rs.getLong("spielId"))
                            .karte(rs.getString("karte"))
                            .von((Integer)rs.getObject("von"))
                            .nach((Integer)rs.getObject("nach"))
                            .zugnummer(rs.getInt("zugnummer"))
                            .spielerwechsel(rs.getBoolean("spielerwechsel"))
                            .zusatzinfo(rs.getString("zusatzinfo"))
                            .erstellt(rs.getTimestamp("erstellt").toInstant())
                            .build();
                    spielzugDTOList.add(spielzugDTO);
                }
                return spielzugDTOList;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public Integer findMaximaleZugnummer(long spielId) {
        var sql = "SELECT max(zugnummer) FROM spielzug WHERE spielid = ?";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql)
        ) {
            stmt.setLong(1, spielId);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return (Integer)rs.getObject(1);
                }
                throw new SQLException("Query 'findMaximaleZugnummer' failed.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
