package eu.dueser.maumau.backend.dao;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.UUID;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

@AllArgsConstructor
@Component
public class SpielerDAO {

    private final DataSource dataSource;

    public UUID insertSpieler(SpielerDTO spiel) {
        var sql = "INSERT INTO spieler (id, name) VALUES (gen_random_uuid(), ?)";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql, RETURN_GENERATED_KEYS)
        ) {
            stmt.setString(1, spiel.getName());

            if (stmt.executeUpdate() == 0) {
                throw new SQLException("Creating 'spieler' failed, no rows affected.");
            }
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getObject("id", UUID.class);
                }
                throw new SQLException("Creating 'spieerl' failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }



//    public void updateSpiel(long id, SpielDTO spiel) {
//        var sql = "UPDATE spiel set start=?,ende=?,gewinnerid=? where id=?";
//
//        try (var conn = dataSource.getConnection();
//             var stmt = conn.prepareStatement(sql)
//        ) {
//            stmt.setTimestamp(1, (spiel.getStart() == null)? null : Timestamp.from(spiel.getStart()));
//            stmt.setTimestamp(2, (spiel.getEnde() == null)? null : Timestamp.from(spiel.getEnde()));
//            stmt.setObject(3, spiel.getGewinnerId());
//
//            if (stmt.executeUpdate() == 0) {
//                throw new SQLException("Update 'spiel' failed, no rows affected.");
//            }
//        } catch (SQLException e) {
//            throw new RuntimeException(e);
//        }
//    }

    public SpielerDTO findSpielerById(UUID id) {
        var sql = "SELECT * FROM spieler WHERE id = ?";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql)
        ) {
            stmt.setObject(1, id);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return SpielerDTO.builder()
                            .id(id)
                            .name(rs.getString("name"))
                            .build();
                }
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    public SpielerDTO findSpielerBySpielerId(UUID id) {
        var sql = "SELECT * FROM spieler WHERE id = ?";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql)
        ) {
            stmt.setObject(1, id);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    return SpielerDTO.builder()
                            .id(id)
                            .name(rs.getString("name"))
                            .build();
                }
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
