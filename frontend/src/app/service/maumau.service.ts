import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {Ablagestapel, FarbeEnum, Karte, KarteUndWunsch, Spielerhand} from "../common/Model";

@Injectable({
  providedIn: 'root'
})
export class MaumauService {

  spielerId = "2bd421cc-4de8-478c-9bc6-cb2fb730da59";

  constructor(private http: HttpClient) {
  }


  getSpielerhand(): Observable<Spielerhand> {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('spielerId', this.spielerId);

    // Wichtig für CORS: Nur relative URLS!
    return this.http.get<Spielerhand>(`/api/spielerhand/1`, {'headers': headers});
  }

  getAblagestapel(): Observable<Ablagestapel> {
    return this.http.get<Ablagestapel>(`/api/ablagestapel/1`);
  }

  ablegen(karte: Karte, wunsch?: FarbeEnum) {
    const headers = new HttpHeaders()
      .set('content-type', 'application/json')
      .set('spielerId', this.spielerId);

    let karteUndWunsch = new KarteUndWunsch(karte, wunsch);
    console.log("Spiele Karte", karteUndWunsch);

    this.http.post<KarteUndWunsch>(`/api/spiel/1/ablegen`, karteUndWunsch, {'headers': headers})
      .subscribe();
    console.log("Karte gespielt");
  }

}
