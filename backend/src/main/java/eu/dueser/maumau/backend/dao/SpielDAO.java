package eu.dueser.maumau.backend.dao;

import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

@AllArgsConstructor
@Component
public class SpielDAO {

    private final DataSource dataSource;

//    public void createSpiel() {
//        var sql = "INSERT INTO Spiel (start,ende,gewinnerid) VALUES ('2001-09-28 01:00', NULL, 5)";
//        try {
//            var conn = dataSource.getConnection();
//            var stmt = conn.createStatement();
//            stmt.executeUpdate(sql);
//            conn.close();
//        } catch (SQLException e) {
//            e.printStackTrace();
//        }
//    }


    public long insertSpiel(SpielDTO spiel) {
        var sql = "INSERT INTO spiel (start,ende,gewinnerid) VALUES (?, ?, ?)";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql, RETURN_GENERATED_KEYS)
        ) {
            stmt.setTimestamp(1, (spiel.getStart() == null)? null : Timestamp.from(spiel.getStart()));
            stmt.setTimestamp(2, (spiel.getEnde() == null)? null : Timestamp.from(spiel.getEnde()));
            stmt.setObject(3, spiel.getGewinnerId());

            if (stmt.executeUpdate() == 0) {
                throw new SQLException("Creating 'spiel' failed, no rows affected.");
            }
            try (ResultSet rs = stmt.getGeneratedKeys()) {
                if (rs.next()) {
                    return rs.getLong(1);
                }
                throw new SQLException("Creating 'spiel' failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void updateSpiel(long id, SpielDTO spiel) {
        var sql = "UPDATE spiel set start=?,ende=?,gewinnerid=? where id=?";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql)
        ) {
            stmt.setTimestamp(1, (spiel.getStart() == null)? null : Timestamp.from(spiel.getStart()));
            stmt.setTimestamp(2, (spiel.getEnde() == null)? null : Timestamp.from(spiel.getEnde()));
            stmt.setObject(3, spiel.getGewinnerId());
            stmt.setLong(4, id);

            if (stmt.executeUpdate() == 0) {
                throw new SQLException("Update 'spiel' failed, no rows affected.");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public SpielDTO findSpielById(long id) {
        var sql = "SELECT * FROM spiel WHERE id = ?";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql)
        ) {
            stmt.setLong(1, id);

            try (ResultSet rs = stmt.executeQuery()) {
                if (rs.next()) {
                    var start = rs.getTimestamp("start");
                    var ende = rs.getTimestamp("ende");
                    var gewinnerId = rs.getObject("gewinnerid");

                    return SpielDTO.builder()
                            .id(id)
                            .start((start == null)? null : start.toInstant())
                            .ende((ende == null)? null : ende.toInstant())
                            .gewinnerId((gewinnerId == null)? null : ((Integer)gewinnerId).longValue())
                            .build();
                }
                return null;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public List<SpielerDTO> findSpielerById(long spielId) {
        var sql = "SELECT spieler.id, spieler.name FROM spiel" +
                " INNER JOIN spiel_spieler ON spiel.id = spiel_spieler.spielid" +
                " INNER JOIN spieler ON spiel_spieler.spielerid = spieler.id" +
                " WHERE spiel.id=? ORDER BY spiel_spieler.position";

        try (var conn = dataSource.getConnection();
             var stmt = conn.prepareStatement(sql)
        ) {
            stmt.setLong(1, spielId);

            try (ResultSet rs = stmt.executeQuery()) {
                var result = new ArrayList<SpielerDTO>();
                while (rs.next()) {
                    result.add(SpielerDTO.builder()
                            .id(rs.getObject("id", UUID.class))
                            .name(rs.getString("name"))
                            .build());
                }
                return result;
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
