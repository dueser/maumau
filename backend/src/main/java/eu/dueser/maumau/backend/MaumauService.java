package eu.dueser.maumau.backend;

import eu.dueser.maumau.backend.exception.SpielBereitsGestartetException;
import eu.dueser.maumau.backend.model.*;
import eu.dueser.maumau.backend.repo.SpielRepository;
import eu.dueser.maumau.backend.repo.SpielSpielerRepository;
import eu.dueser.maumau.backend.repo.SpielerRepository;
import eu.dueser.maumau.backend.repo.SpielzugRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

import java.time.Instant;

import static eu.dueser.maumau.backend.model.SpielMaschine.*;

@Service
@AllArgsConstructor
@Transactional
public class MaumauService {

    public final SpielRepository spielRepository;

    public final SpielzugRepository spielzugRepository;

    public final SpielerRepository spielerRepository;

    public final SpielSpielerRepository spielSpielerRepository;

    public Spiel neuesSpiel() {
        return spielRepository.create();
    }


    public Spiel getSpiel(long spielId) {
        return spielRepository.getNotNull(spielId);
    }

    public Spieler neuerSpieler(long spielId, String name) {
        var spiel = spielRepository.getNotNull(spielId);
        if (spiel.getStart() != null) {
            throw new SpielBereitsGestartetException(spielId);
        }
        var spieler = spielerRepository.create(name);
        spielSpielerRepository.linkSpielSpieler(spielId, spieler.getId());
        return spieler;
    }

    public Spiel spielStarten(long spielId) {
        var spiel = spielRepository.getNotNull(spielId);
        spiel.setStart(Instant.now());
        spielRepository.update(spiel);

        var spielerzahl = spiel.getSpielerliste().size();
        if (spielerzahl < SPIELER_ANZAHL_MIN || spielerzahl > SPIELER_ANZAHL_MAX) {
            throw new IllegalStateException("Ungültige Spieleranzahl " + spielerzahl + " bei Spiel " + spielId);
        }

        var spielMaschine = new SpielMaschine(getSpielzustand(spielId));
        spielMaschine.austeilen(spielId, spielerzahl).forEach(spielzugRepository::add);

        return spiel;
    }

    public Karte ziehen(long spielId, UUID spielerId) {
        var spielMaschine = new SpielMaschine(getSpielzustand(spielId));
        var spielzug = spielMaschine.ziehen(spielerId);
        spielzugRepository.add(spielzug);
        return spielzug.getKarte();
    }

    public void ablegen(long spielId, UUID spielerId, Karte karte, FarbeEnum wunschfarbe) {
        var spielMaschine = new SpielMaschine(getSpielzustand(spielId));
        var spielzug = spielMaschine.ablegen(spielerId, karte, wunschfarbe);
        spielzugRepository.add(spielzug);
    }

    public void weiter(long spielId, UUID spielerId) {
        var spielMaschine = new SpielMaschine(getSpielzustand(spielId));
        var spielzug = spielMaschine.weiter(spielerId);
        spielzugRepository.add(spielzug);
    }


    public Spielzustand getSpielzustand(long spielId) {
        var spiel = spielRepository.getNotNull(spielId);
        var spielzustand = new Spielzustand(spiel);
        var spielzuege = spielzugRepository.findSpielzuegeBySpielId(spielId);
        for (var spielzug : spielzuege) {
            spielzustand.applySpielzug(spielzug);
        }
        return spielzustand;
    }
}
