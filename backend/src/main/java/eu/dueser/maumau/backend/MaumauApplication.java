package eu.dueser.maumau.backend;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.transaction.annotation.EnableTransactionManagement;

@SpringBootApplication
@EnableTransactionManagement
public class MaumauApplication {

    public static void main(String[] args) {
        SpringApplication.run(MaumauApplication.class, args);
    }

}
