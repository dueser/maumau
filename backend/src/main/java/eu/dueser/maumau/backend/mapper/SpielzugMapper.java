package eu.dueser.maumau.backend.mapper;

import eu.dueser.maumau.backend.dao.SpielzugDTO;
import eu.dueser.maumau.backend.model.Karte;
import eu.dueser.maumau.backend.model.Spielzug;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.Named;

@Mapper(componentModel = "spring")
public interface SpielzugMapper {


    @Mapping(source = "karte", target = "karte", qualifiedByName = "karteToString")
    SpielzugDTO spielzugToSpielzugDTO(Spielzug spiel);

    @Named("karteToString")
    default String karteToString(Karte karte) {
        return karte.toString();
    }

    @Mapping(source = "karte", target = "karte", qualifiedByName = "stringToKarte")
    Spielzug spielzugDTOToSpielzug(SpielzugDTO spiel);

    @Named("stringToKarte")
    default Karte stringToKarte(String karte) {
        return new Karte(karte);
    }

}
