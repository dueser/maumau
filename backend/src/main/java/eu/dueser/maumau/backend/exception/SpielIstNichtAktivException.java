package eu.dueser.maumau.backend.exception;

public class SpielIstNichtAktivException extends IllegalStateException {

    public SpielIstNichtAktivException(long spielId) {
        super("Das Spiel mit der Id " + spielId + " ist nicht aktiv.");
    }
}
