package eu.dueser.maumau.backend.model;

import lombok.Data;

import java.time.Instant;
import java.util.List;

@Data
public class Spiel {
    private Long id;
    private Instant start;
    private Instant end;
    private Long gewinnerId;
    private List<Spieler> spielerliste;

    public boolean istGestartet() {
        return start != null;
    }

    public boolean istBeendet() {
        return end != null;
    }

    public boolean istAktiv() {
        return istGestartet() && !istBeendet();
    }

}



