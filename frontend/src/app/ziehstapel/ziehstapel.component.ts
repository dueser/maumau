import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-ziehstapel',
  templateUrl: './ziehstapel.component.html',
  styleUrls: ['./ziehstapel.component.css']
})
export class ZiehstapelComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {}

  zieheKarte() {
    console.log("Ziehe Karte");
  }
}
