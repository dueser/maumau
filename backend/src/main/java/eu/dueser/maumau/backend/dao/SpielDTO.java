package eu.dueser.maumau.backend.dao;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class SpielDTO {
    private Long id;

    private Instant start;

    private Instant ende;

    private Long gewinnerId;

}



