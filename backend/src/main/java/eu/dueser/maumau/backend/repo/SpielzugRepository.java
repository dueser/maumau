package eu.dueser.maumau.backend.repo;

import eu.dueser.maumau.backend.dao.SpielzugDAO;
import eu.dueser.maumau.backend.mapper.SpielzugMapper;
import eu.dueser.maumau.backend.model.Spielzug;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.List;

@Component
@AllArgsConstructor
public class SpielzugRepository {

    private final SpielzugDAO spielzugDAO;
    private final SpielzugMapper spielzugMapper;


    public long insert(Spielzug spielzug) {
        if (spielzug.getZugnummer() == null) {
            throw new IllegalArgumentException("Property 'zugnummer fehlt bei: " + spielzug);
        }
        var spielzugDTO = spielzugMapper.spielzugToSpielzugDTO(spielzug);
        return spielzugDAO.insertSpielzug(spielzugDTO);
    }

    public long add(Spielzug spielzug) {
        if (spielzug.getZugnummer() != null) {
            throw new IllegalArgumentException("Property 'zugnummer darf nicht gesetzt sein bei: " + spielzug);
        }
        var maximaleZugnummer = spielzugDAO.findMaximaleZugnummer(spielzug.getSpielId());
        spielzug.setZugnummer((maximaleZugnummer == null)? 0 : maximaleZugnummer + 1);
        return insert(spielzug);
    }

    public List<Spielzug> findSpielzuegeBySpielId(long spielId) {
        return spielzugDAO.findSpielzuegeBySpielId(spielId).stream()
                .map(spielzugMapper::spielzugDTOToSpielzug)
                .toList();
    }

}
