package eu.dueser.maumau.backend.model;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class Karte {
    
    public Karte(String karteAsStr) {
        var split = karteAsStr.split("-");
        farbe = FarbeEnum.valueOf(split[0]);
        wert = WertEnum.valueOf(split[1]);
    }

    private final WertEnum wert;
    private final FarbeEnum farbe;

    @Override
    public String toString() {
        return farbe + "-" + wert;
    }
    

    public static void main(String[] args) {
        var karte = new Karte(new Karte(WertEnum.K, FarbeEnum.HERZ).toString());
        System.out.println(karte);
    }

}
