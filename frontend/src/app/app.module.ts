import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { SpielerhandComponent } from './spielerhand/spielerhand.component';
import { KarteComponent } from './karte/karte.component';
import { AblagestapelComponent } from './ablagestapel/ablagestapel.component';
import {ZiehstapelComponent} from "./ziehstapel/ziehstapel.component";

@NgModule({
  declarations: [
    AppComponent,
    SpielerhandComponent,
    KarteComponent,
    AblagestapelComponent,
    ZiehstapelComponent
  ],
  imports: [
    BrowserModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
