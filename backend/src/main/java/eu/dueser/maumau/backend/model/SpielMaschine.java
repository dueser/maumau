package eu.dueser.maumau.backend.model;

import eu.dueser.maumau.backend.exception.SpielIstNichtAktivException;
import lombok.Data;

import java.time.Instant;
import java.util.ArrayList;
import java.util.UUID;

import static eu.dueser.maumau.backend.model.Spielzustand.INDEX_ABLEGESTAPEL;
import static eu.dueser.maumau.backend.model.Spielzustand.INDEX_ZIEHSTAPEL;
import static eu.dueser.maumau.backend.model.WertEnum.B;

@Data
public class SpielMaschine {


    public final static int SPIELER_ANZAHL_MIN = 2;
    public final static int SPIELER_ANZAHL_MAX = 4;
    public final static int KARTEN_PRO_SPIELER = 5;

    private final Spielzustand spielzustand;

    public SpielMaschine(Spielzustand spielzustand) {
        this.spielzustand = spielzustand;
    }

    public static class SpielerHatKarteNicht extends IllegalArgumentException {}

    public static class SpielerIstNichtAmZug extends IllegalArgumentException {}

    /**
     * @param karte klar
     * @param wunschfarbeBeiBauer, null bei allen anderen Karten
     */
    public Spielzug ablegen(UUID spielerId, Karte karte, FarbeEnum wunschfarbeBeiBauer) {
        validiere(spielerId);
        // überprüfen, ob Spieler Karte besitzt:
        if (!spielzustand.getSpielerhand(spielerId).istEnthalten(karte)) {
            throw new SpielerHatKarteNicht();
        }
        var zusatzinfo = (karte.getWert() == B)? "Wunsch:" + wunschfarbeBeiBauer: null;
        var meinePosition = spielzustand.getPositionAktSpieler();

        return Spielzug.builder()
                .spielId(spielzustand.getSpiel().getId())
                .erstellt(Instant.now())
                .von(meinePosition)
                .nach(INDEX_ABLEGESTAPEL)
                .karte(karte)
                .spielerwechsel(true)
                .zusatzinfo(zusatzinfo)
                .build();
    }

    /**
     */
    public Spielzug ziehen(UUID spielerId) {
        validiere(spielerId);
        var meinePosition = spielzustand.getPositionAktSpieler();
        var oberste = spielzustand.getZiehstapel().oberste();
        if (oberste == null) {
            // FIXME umschiften
        }
        return Spielzug.builder()
                .spielId(spielzustand.getSpiel().getId())
                .erstellt(Instant.now())
                .von(INDEX_ZIEHSTAPEL)
                .nach(meinePosition)
                .karte(spielzustand.getZiehstapel().oberste())
                .spielerwechsel(false)
                .build();
    }

    public Spielzug weiter(UUID spielerId) {
        validiere(spielerId);
        return Spielzug.builder()
                .spielId(spielzustand.getSpiel().getId())
                .erstellt(Instant.now())
                .spielerwechsel(true)
                .build();
    }

    private void validiere(UUID spielerId) {
        if (!spielzustand.getSpiel().istAktiv()) {
            throw new SpielIstNichtAktivException(spielzustand.getSpiel().getId());
        }
        // ist Spieler mit spielerId überhaupt am Zug?
        if (!spielzustand.aktuellerSpieler().getId().equals(spielerId)) {
            throw new SpielerIstNichtAmZug();
        }
    }

    public ArrayList<Spielzug> austeilen(long spielId, int spielerzahl) {
        var spielzuege = new ArrayList<Spielzug>();
        var kartensammlung = Kartensammlung.neuesGemischtenSpiel();

        for (int k = 0; k < KARTEN_PRO_SPIELER; k++) {
            for (int i = 0; i < spielerzahl; i++) {
                var spielzug = Spielzug.builder()
                        .erstellt(Instant.now())
                        .spielId(spielId)
                        .nach(i)
                        .karte(kartensammlung.entnehmen())
                        .build();
                spielzuege.add(spielzug);
            }
        }

        // bis auf eine Karte alles in den Ziehstapel
        while (kartensammlung.anzahl() > 1) {
            var spielzug = Spielzug.builder()
                    .erstellt(Instant.now())
                    .spielId(spielId)
                    .nach(INDEX_ZIEHSTAPEL)
                    .karte(kartensammlung.entnehmen())
                    .build();
            spielzuege.add(spielzug);
        }

        // die letzte Karte auf den Ablegestapel
        var spielzug = Spielzug.builder()
                .erstellt(Instant.now())
                .spielId(spielId)
                .nach(INDEX_ABLEGESTAPEL)
                .karte(kartensammlung.entnehmen())
                .build();
        spielzuege.add(spielzug);

        return spielzuege;
    }




//        var obersteKarteAblagestapel = spielzustand.getAblagestapel().oberste();
//
//        switch (karte.getWert()) {
//            case B:
//                ablagefarbe = wunschfarbeBeiBauer;
//                karteVonSpielerAufAblagestapel(spielerId, karte);
//                aktualisiereSpieler(karte);
//                break;
//            case Z7:
//
//                break;
//            default:
//                karteVonSpielerAufAblagestapel(spielerId, karte);
//                aktualisiereSpieler(karte);
//        }
//
//
//        if (karte.getWert() == WertEnum.B){
//            //
//        }
//        else if(karte.getWert() == obersteKarteAblagestapel.getWert()
//                || karte.getFarbe() == obersteKarteAblagestapel.getFarbe()) {
//            karteVonSpielerAufAblagestapel(spielerId, karte);
//
//        }
//        else throw new KartePasstNicht();
//
//        // ist der zug gültig -> wenn nicht exception
//        // wenn gültig -> karte von spielerhand entfernen und ablagestapel transaktional hinzufügen
//
//
//
//    }
//
//    private void aktualisiereSpieler(Karte karte) {
//        int offset;
//        switch (karte.getWert()) {
//            case Z8:
//                offset = imUhrzeigersinn? 2 : -2;
//                break;
//            case Z9:
//                imUhrzeigersinn = !imUhrzeigersinn;
//                offset = imUhrzeigersinn? 1 : -1;
//                break;
//            default:
//                offset = imUhrzeigersinn? 1 : -1;
//        }
//        aktuellerSpieler = (aktuellerSpieler + offset) % anzahlSpieler();
//    }
//
//    private void karteVonSpielerAufAblagestapel(Long spielerId, Karte karte) {
//        spielerIdToKartensammlung.get(spielerId).entfernen(karte);
//        ablagestapel.hinzufuegen(karte);
//    }
//
//    public void spielerZiehtVomZiehstapel(Long spielerId){
//        var gezogeneKarte = ziehstapel.oberste();
//        ziehstapel.entnehmen();
//        spielerIdToKartensammlung.get(spielerId).hinzufuegen(gezogeneKarte);
//
//        // oberste karte wird vom ziehstapel entfernt und der Spielerhand transaktional hinzugefügt
//
//    }

}



