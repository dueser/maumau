package eu.dueser.maumau.backend.exception;

public class SpielBereitsGestartetException extends IllegalStateException {

    public SpielBereitsGestartetException(long spielId) {
        super("Das Spiel mit der Id " + spielId + " läuft bereits.");
    }
}
