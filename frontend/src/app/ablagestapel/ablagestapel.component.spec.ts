import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AblagestapelComponent } from './ablagestapel.component';

describe('AblagestapelComponent', () => {
  let component: AblagestapelComponent;
  let fixture: ComponentFixture<AblagestapelComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AblagestapelComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AblagestapelComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
