package eu.dueser.maumau.backend.repo;

import eu.dueser.maumau.backend.dao.SpielDAO;
import eu.dueser.maumau.backend.exception.SpielExistiertNichtException;
import eu.dueser.maumau.backend.mapper.SpielMapper;
import eu.dueser.maumau.backend.mapper.SpielerMapper;
import eu.dueser.maumau.backend.model.Spiel;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SpielRepository {

    private final SpielDAO spielDAO;
    private final SpielMapper spielMapper;
    private final SpielerMapper spielerMapper;

    public Spiel get(long id) {
        var spielDTO = spielDAO.findSpielById(id);
        if (spielDTO == null) return null;
        var spiel = spielMapper.spielDTOToSpiel(spielDTO);
        spiel.setSpielerliste(
                spielDAO.findSpielerById(id).stream()
                .map(spielerDTO -> spielerMapper.spielerDTOToSpieler(spielerDTO))
                .toList());

        return spiel;
    }

    public Spiel getNotNull(long id) {
        var spiel = get(id);
        if (spiel == null) {
            throw new SpielExistiertNichtException(id);
        }
        return spiel;
    }

    public Spiel create() {
        var spiel = new Spiel();
        spiel.setId(add(spiel));
        return spiel;
    }

    public long add(Spiel spiel) {
        if (spiel.getId() != null) {
            throw new IllegalArgumentException("Spiel darf keine Id enthalten:" + spiel.getId());
        }
        var spielDTO = spielMapper.spielToSpielDTO(spiel);
        return spielDAO.insertSpiel(spielDTO);
    }

    public void update(Spiel spiel) {
        if (spiel.getId() == null) {
            throw new IllegalArgumentException("Spiel muss eine Id enthalten.");
        }
        var spielDTO = spielMapper.spielToSpielDTO(spiel);
        spielDAO.updateSpiel(spiel.getId(), spielDTO);
    }

//    public void findSpielerBySpielId(long spielId) {
//        spielDAO.findSpielerById()
//    }

}
