export class Karte {
  constructor(wert: WertEnum, farbe: FarbeEnum) {
    this.wert = wert;
    this.farbe = farbe;
  }
  wert: WertEnum;
  farbe: FarbeEnum;
}

export enum WertEnum {
  Z7= 'Z7', Z8 = 'Z8', Z9 = 'Z9', Z10 = 'Z10', B = 'B', D = 'D', K = 'K', A = 'A'
}

export enum FarbeEnum {
  KARO = 'KARO', HERZ = 'HERZ', PIK = 'PIK', KREUZ = 'KREUZ'
}

export class Spielerhand {

  constructor(karten: Karte[]) {
    this.karten = karten;
  }
  karten : Karte[];
}

export class Ablagestapel {

  constructor(karten: Karte[]) {
    this.karten = karten;
  }
  karten : Karte[];
}

export class Ziehstapel {

  constructor(karten: Karte[]) {
    this.karten = karten;
  }
  karten : Karte[];
}


export class KarteUndWunsch {

  constructor(karte: Karte, wunsch?: FarbeEnum) {
    this.karte = karte;
    this.wunsch = wunsch;
  }
  karte: Karte;
  wunsch?: FarbeEnum;

}
