package eu.dueser.maumau.backend.model;

import lombok.Data;

import java.util.UUID;

@Data
public class Spieler {
    private UUID id;
    private String name;
}

