docker pull postgres
docker run --name maumau-postgres -p 5432:5432 -e POSTGRES_USER=postgres -e POSTGRES_PASSWORD=postgres -d postgres
docker exec maumau-postgres psql -U postgres -c"CREATE DATABASE maumaudb" postgres