package eu.dueser.maumau.backend.model;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class Spielzug {

    private Long spielId;
    private Karte karte;
    private Integer von;
    private Integer nach;
    private Integer zugnummer;
    private boolean spielerwechsel;
    private String zusatzinfo;
    private Instant erstellt;

}
