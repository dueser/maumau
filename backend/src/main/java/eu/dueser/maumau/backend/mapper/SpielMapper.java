package eu.dueser.maumau.backend.mapper;

import eu.dueser.maumau.backend.dao.SpielDTO;
import eu.dueser.maumau.backend.model.Spiel;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "spring")
public interface SpielMapper {

    SpielDTO spielToSpielDTO(Spiel spiel);

    // @Mapping(target = "spielerliste", ignore = true)
    Spiel spielDTOToSpiel(SpielDTO spiel);

}
