package eu.dueser.maumau.backend.repo;

import eu.dueser.maumau.backend.dao.SpielSpielerDAO;
import eu.dueser.maumau.backend.dao.SpielSpielerDTO;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
@AllArgsConstructor
public class SpielSpielerRepository {

    private final SpielSpielerDAO spielSpielerDAO;

    public void linkSpielSpieler(long spielId, UUID spielerId) {

        var spielerBySpielId = spielSpielerDAO.findSpielSpielerBySpielId(spielId);

        var spielSpielerDTO = SpielSpielerDTO.builder()
                .spielId(spielId)
                .spielerId(spielerId)
                .position(spielerBySpielId.size())
                .build();
        spielSpielerDAO.insertSpielSpieler(spielSpielerDTO);
    }

}
