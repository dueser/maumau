package eu.dueser.maumau.backend;

import eu.dueser.maumau.backend.model.*;
import lombok.AllArgsConstructor;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.UUID;

@CrossOrigin(origins = "http://localhost")
@RestController
@RequestMapping("/api")
@AllArgsConstructor
public class MaumauController {

    private final MaumauService maumauService;

    /**
     * Erzeugt ein neues Spiel. Die eindeutige Id wird innerhalb des Rückgabeobjekts
     * zurückgeliefert.
     * @return das {@link Spiel}
     */
    @PostMapping(path = "/spiel", produces = MediaType.APPLICATION_JSON_VALUE)
    Spiel neuesSpiel() {
        return maumauService.neuesSpiel();
    }

    /**
     * Fügt einen neuen Spieler mit dem Namen <code>spielername</code> zum Spiel
     * mit der Id <code>spielId</code> hinzu.
     * Dies funktioniert nur, solange das Spiel noch nicht gestartet ist.
     * @param spielId Id des Spiels
     * @param spielername Name des Spielers
     * @return der {@link Spieler}
     */
    @PostMapping(path = "/spieler/{spielId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Spieler neuerSpieler(@PathVariable long spielId, @RequestBody String spielername) {
        return maumauService.neuerSpieler(spielId, spielername);
    }

    /**
     * Start das Spiel.
     * @param spielId Id des Spiels
     * @return das {@link Spiel}
     */
    @PostMapping(path = "/spiel/{spielId}/starten", produces = MediaType.APPLICATION_JSON_VALUE)
    Spiel spielStarten(@PathVariable long spielId) {
        return maumauService.spielStarten(spielId);
    }


    @GetMapping(path = "/spielerhand/{spielId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Kartensammlung getSpielerhand(@PathVariable long spielId, @RequestHeader("spielerId") String spielerId) {
        return maumauService.getSpielzustand(spielId).getSpielerhand(UUID.fromString(spielerId));
    }

    @GetMapping(path = "/ablagestapel/{spielId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Kartensammlung getAblagestapel(@PathVariable long spielId) {
        return maumauService.getSpielzustand(spielId).getAblagestapel();
    }

    @GetMapping(path = "/spielzustand/{spielId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Spielzustand getSpielzustand(@PathVariable long spielId) {
        return maumauService.getSpielzustand(spielId);
    }


    @GetMapping(path = "/spiel/{spielId}", produces = MediaType.APPLICATION_JSON_VALUE)
    Spiel getSpiel(@PathVariable long spielId) {
        return maumauService.getSpiel(spielId);
    }

    @PostMapping(path = "/spiel/{spielId}/ziehen", produces = MediaType.APPLICATION_JSON_VALUE)
    Karte ziehen(@PathVariable long spielId, @RequestHeader("spielerId") String spielerId) {
        return maumauService.ziehen(spielId, UUID.fromString(spielerId));
    }

    public record KarteUndWunsch(Karte karte, FarbeEnum wunschfarbe) {}

    @PostMapping(path = "/spiel/{spielId}/ablegen", produces = MediaType.APPLICATION_JSON_VALUE)
    void ablegen(@PathVariable long spielId, @RequestBody KarteUndWunsch karteUndWunsch, @RequestHeader("spielerId") String spielerId) {
        maumauService.ablegen(spielId, UUID.fromString(spielerId), karteUndWunsch.karte, karteUndWunsch.wunschfarbe);
    }

    @PostMapping(path = "/spiel/{spielId}/weiter", produces = MediaType.APPLICATION_JSON_VALUE)
    void weiter(@PathVariable long spielId, @RequestHeader("spielerId") String spielerId) {
        maumauService.weiter(spielId, UUID.fromString(spielerId));
    }

}
