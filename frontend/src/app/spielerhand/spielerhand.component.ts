import {Component, OnInit} from '@angular/core';
import {Karte, Spielerhand} from "../common/Model";
import {MaumauService} from "../service/maumau.service";
import {map, Observable} from "rxjs";

@Component({
  selector: 'app-spielerhand',
  templateUrl: './spielerhand.component.html',
  styleUrls: ['./spielerhand.component.css']
})
export class SpielerhandComponent implements OnInit {

  spielerhand$: Observable<Spielerhand>;
  karten$: Observable<Karte[]>;

  constructor(private maumauService: MaumauService) {
    this.spielerhand$ = maumauService.getSpielerhand();
    this.karten$ = this.spielerhand$.pipe(map(spielerhand => spielerhand.karten))
  }

  ngOnInit(): void {
  }

  ablegen(karte: Karte) {
    this.maumauService.ablegen(karte);
  }
}
