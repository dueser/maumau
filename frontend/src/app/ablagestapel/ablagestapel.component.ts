import { Component, OnInit } from '@angular/core';
import {MaumauService} from "../service/maumau.service";
import {map, Observable} from "rxjs";
import {Ablagestapel, Karte} from "../common/Model";

@Component({
  selector: 'app-ablagestapel',
  templateUrl: './ablagestapel.component.html',
  styleUrls: ['./ablagestapel.component.css']
})
export class AblagestapelComponent implements OnInit {

  ablagestapel$ : Observable<Ablagestapel>;
  obersteKarte$: Observable<Karte>;

  constructor(private maumauService: MaumauService) {
    this.ablagestapel$ = maumauService.getAblagestapel();

    this.obersteKarte$ = this.ablagestapel$.pipe(
      map((ablagestapel:Ablagestapel) =>
        ablagestapel.karten[ablagestapel.karten.length - 1] as Karte));
  }

  ngOnInit(): void {
    // this.ablagestapel$.subscribe(a => console.log("Ablagestapel: ",a))
    // this.obersteKarte$.subscribe(a => console.log("Oberste Karte: ",a))
  }

}
