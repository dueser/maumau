package eu.dueser.maumau.backend.dao;

import lombok.Builder;
import lombok.Data;

import java.time.Instant;

@Data
@Builder
public class SpielzugDTO {
    private Long spielId;

    private String karte;

    private Integer von;

    private Integer nach;

    private int zugnummer;

    private boolean spielerwechsel;

    private String zusatzinfo;

    private Instant erstellt;

}



