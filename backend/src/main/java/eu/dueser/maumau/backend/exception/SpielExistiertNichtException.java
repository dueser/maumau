package eu.dueser.maumau.backend.exception;

public class SpielExistiertNichtException extends ResourceNotFoundException {

    public SpielExistiertNichtException(long spielId) {
        super("Spiel existiert nicht für Id " + spielId);
    }

}
