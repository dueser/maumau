import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Karte} from "../common/Model";



@Component({
  selector: 'app-karte',
  templateUrl: './karte.component.html',
  styleUrls: ['./karte.component.css']
})
export class KarteComponent implements OnInit {

  @Input()
  karte? : Karte;
  @Input()
  klickbar?: boolean;
  @Output() kartenEvent = new EventEmitter<Karte>();

  imagePath? : string;

  constructor() { }

  ngOnInit(): void {
    console.log("Karte", this.karte) // <= null
    this.imagePath = `assets/${this.karte?.farbe}_${this.karte?.wert}.png`;
  }

  ablegen() {
    this.kartenEvent.emit(this.karte);
  }
}
