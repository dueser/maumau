import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SpielerhandComponent } from './spielerhand.component';

describe('SpielerhandComponent', () => {
  let component: SpielerhandComponent;
  let fixture: ComponentFixture<SpielerhandComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SpielerhandComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SpielerhandComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
