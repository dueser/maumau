package eu.dueser.maumau.backend.repo;

import eu.dueser.maumau.backend.dao.SpielerDAO;
import eu.dueser.maumau.backend.dao.SpielerDTO;
import eu.dueser.maumau.backend.mapper.SpielerMapper;
import eu.dueser.maumau.backend.model.Spieler;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Component;

@Component
@AllArgsConstructor
public class SpielerRepository {

    private final SpielerMapper spielerMapper;

    private final SpielerDAO spielerDAO;

    public Spieler create(String name) {
        var spielerDTO = SpielerDTO.builder().name(name).build();
        var id = spielerDAO.insertSpieler(spielerDTO);
        var spieler = spielerMapper.spielerDTOToSpieler(spielerDTO);
        spieler.setId(id);
        return spieler;
    }

}
