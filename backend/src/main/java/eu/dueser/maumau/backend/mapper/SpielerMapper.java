package eu.dueser.maumau.backend.mapper;

import eu.dueser.maumau.backend.dao.SpielerDTO;
import eu.dueser.maumau.backend.model.Spieler;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface SpielerMapper {

    SpielerDTO spielerToSpielerDTO(Spieler spieler);

    Spieler spielerDTOToSpieler(SpielerDTO spielerDTO);

}
