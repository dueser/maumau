package eu.dueser.maumau.backend.dao;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class SpielerDTO {

    private UUID id;

    private String name;

}
